"""iTEBD.py
   Simple first attempt to implement arXiv:cond-mat/0605597v2 (Vidal 2008)
   in Python.
"""
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as la
from scipy import linalg as spla
import scipy.special as spe
from scon import scon
import cPickle as pickle

#Global variables
CHI = 20   #the bond dimension
D   = 2   #the Hilbert space dimension

T0 = 0.   #initial time
TF = 20.   #final time
DT = 0.0001  #timestep
DT_OBS = 0.1 #how often to compute observables
DEBUG = False
#FROMFILE = True
#ICFILE = "arrays"

#Physical variables
INTERACTION_FACTOR_J = 1.00    
TRANSVERSE_FIELD_FACTOR_h = 1.2 

#Pauli matrices
Sig_x = np.array([ [0, 1],
                   [1, 0]] )

Sig_y = np.array([ [0, -1.j],
                   [1.j, 0]] )

Sig_z = np.array([ [1,  0],
                   [0, -1]] )


def Uexp(U):
    """Compute exp(U).
       Where U has shape (2, 2, 2, 2).
    """
    Umat = U.reshape((4, 4))
    Umatexp = spla.expm(Umat)
    Uexp = Umatexp.reshape((2, 2, 2, 2,))
    return Uexp

#The Hamiltonian
def U_t(t, J=INTERACTION_FACTOR_J, h=TRANSVERSE_FIELD_FACTOR_h):
    U = -J * (np.kron(Sig_x, Sig_x) + h * np.kron(Sig_z, np.eye(2)))
    Ushpd = U.reshape((2,2,2,2))
    Ushpd = np.einsum('klij',Ushpd)
    return Ushpd#U.reshape((2, 2, 2, 2))

def Utexactenergy(J=INTERACTION_FACTOR_J, h=TRANSVERSE_FIELD_FACTOR_h):
    """Exact ground state energy for the transverse uniform Ising model.
    """
    lam = J/h
    E = -h * 2/np.pi * (1 + lam) * spe.ellipe((4*lam/(1+lam)**2))
    return E

def U_eye(t):
    eye = np.kron(np.eye(2), np.eye(2)).reshape((2,2,2,2))
    return eye

class iTEBD:
    """Class to implement the TEBD. Takes a Hamiltonian, and an MPS. 
       Method 'evolve' applies the former to the latter and produces
       output.
    """
    def __init__(self, iMPS, U_t, h=TRANSVERSE_FIELD_FACTOR_h): 
        self.iMPS = iMPS
        self.U_t = U_t
        self.h = h

    def observables(self, t, U, printmode=True): 
        brath, ketth = self.iMPS.braketcontract()
        pre = [brath, ketth]

        energy = self.iMPS.hamp(U, pre)
        Sxexp = self.iMPS.S1amp(Sig_x, pre)
        Syexp = self.iMPS.S1amp(Sig_y, pre)
        Szexp = self.iMPS.S1amp(Sig_z, pre)
        C2 = self.iMPS.correlator(pre)

        Eexact = Utexactenergy(h=self.h)
        dE = energy - Eexact
        
        outstring = "t = " + str(t) + "| E = " + str(energy)
        outstring += " | dE = " + str(dE)
        outstring += "| Sx = " + str(Sxexp)
        outstring += "| Sy = " + str(Syexp)
        outstring += "| Sz = " + str(Szexp)
        outstring += "| C2 = " + str(C2)
        return [outstring, t, energy, Sxexp, Syexp, Szexp, dE, C2]
    
    def append_arrays(self, arrays, new):
        arrays["t"].append(new[0])
        arrays["E"].append(new[1])
        arrays["Sx"].append(new[2])
        arrays["Sy"].append(new[3])
        arrays["Sz"].append(new[4])
        arrays["dE"].append(new[5])
        arrays["C2"].append(new[6])
    
    def evolve(self, t0, tf, delta_t):
        arrays = dict()
        arrays["t"] = []
        arrays["E"] = []
        arrays["Sx"] = []
        arrays["Sy"] = []
        arrays["Sz"] = []
        arrays["dE"] = []
        arrays["C2"] = []
        t=t0
        print "BEGINNING EVOLUTION..."
        print "\t t0 = ", t, " | tf = ", tf, " | delta_t = ", delta_t
        print "Exact energy: " + str(Utexactenergy(h=self.h))
        print "_____________________________________________________"
        H = self.U_t(0, h=self.h)
        U = Uexp(-H*delta_t)
        obs = self.observables(t, H)
        print obs[0]
        self.append_arrays(arrays, obs[1:])
        outtime = DT_OBS
        while t<tf:
          self.iMPS.evolve(U)
          t+=delta_t
          if t>=outtime:
            obs = self.observables(t, H)
            print obs[0]
            self.append_arrays(arrays, obs[1:])
            outtime += DT_OBS

        return arrays
        # dEarr = [E-Utexactenergy() for E in E_arr]
          #raise ValueError

class iMPS:
  """Represents two sites of an infinite quantum lattice as an MPS.
     This involves: 
        -the Hilbert space dimension d 
        -the bond dimension chi
        -the maximum bond dimension max_chi
        -two length-d vectors lambda_A and lambda_B
        -two chi x d x chi tensors Gamma_A and Gamma_B
      Arranged as lambda_b - Gamma_A - lambda_a - Gamma_B - lambda_b.
  """
  def __init__(self, d, chi, max_chi, lambda_A, lambda_B, Gamma_A, Gamma_B,
               debug=DEBUG):
      self.d = d
      self.chi = chi
      self.max_chi = max_chi
      self.lambda_B = lambda_B
      self.debug = debug
      self.update(Gamma_A, lambda_A, Gamma_B)
      self.debugstrings()

  def debugstrings(self):
      if self.debug:
        norm = la.norm(self.lambda_A)
        print "[DEBUG] norm(lambda_A)==1? -> ", np.allclose([1], [norm]), " (", norm, ")" 
        normB = la.norm(self.lambda_B)
        print "[DEBUG] norm(lambda_B)==1? -> ", np.allclose([1], [normB]), " (", normB, ")" 
        amp = self.amplitude()
        print "[DEBUG] |<psi|psi>|==1? -> ", np.allclose([1], [amp]), " (", amp, ")"

  def update(self, Gamma_A, lambda_A, Gamma_B):
      self.Gamma_A = Gamma_A 
      self.lambda_A = lambda_A
      self.Gamma_B = Gamma_B 
      self.normalize()

  def normalize(self):
      """Rescale Gamma_A to enforce that the norm <psi|psi>=1.
         Note the norm is quadratic in an overall scaling of Gamma_A,
         so we do
          Gamma_A -> c_a Gamma_A
            where c_a = 1/sqrt(<psi|psi>).
        This method also caches the contracted bra and ket networks, for
        later use in computing amplitudes.
      """
      norm = self.amplitude()
      c_a = 1./np.sqrt(norm)
      self.Gamma_A *= c_a

  def updateandcycle(self, lambda_B, Gamma_A, lambda_A, Gamma_B):
      self.Gamma_A = Gamma_B
      self.lambda_B = lambda_A
      self.Gamma_B = Gamma_A
      self.lambda_A = lambda_B
      self.normalize()
  
  def evolve(self, U):
      my_evolver = iMPS_Evolver(self, U)
      newGamA, newlamA, newGamB = my_evolver.evolve()
      self.updateandcycle(self.lambda_B, newGamA, newlamA, newGamB)

      my_evolver = iMPS_Evolver(self, U)
      newGamA, newlamA, newGamB = my_evolver.evolve()
      self.updateandcycle(self.lambda_B, newGamA, newlamA, newGamB)

      self.debugstrings()

  def contract_network(self, lamB, GamA, lamA, GamB):
      left = (lamB * GamA.T).T
      left *= lamA
      right = GamB * lamB
      full = np.einsum('abc, cde', left, right)
      return full

  def braketcontract(self, left=True):
      if left:
        lamB = self.lambda_B
        GamA = self.Gamma_A
        lamA = self.lambda_A
        GamB = self.Gamma_B
      else:
        lamB = self.lambda_A
        GamA = self.Gamma_B
        GamB = self.Gamma_A
        lamA = self.lambda_B

      ketth = self.contract_network(lamB, GamA, lamA, GamB)
      brath = self.contract_network(lamB, np.conj(GamA), lamA, np.conj(GamB))

      return brath, ketth

  def getbraket(self, pre=None, debug=False):
      if pre is not None: 
        brath, ketth = pre
        if debug:
          brath2, ketth2 = self.braketcontract()
          print "[DEBUG] Stored bra correct: ", np.allclose(brath, brath2)
          print "[DEBUG] Stored ket correct: ", np.allclose(ketth, ketth2)
      else:
        brath, ketth = self.braketcontract()
      return brath, ketth
      

  def amplitude(self, pre=None):
      """Compute <psi|psi>
      """
      brath, ketth = self.getbraket(pre, self.debug)
      expect = scon([brath, ketth], ([1, 2, 3, 4], 
                                     [1, 2, 3, 4]))
      return expect.real

  def contract(self):
      

  def correlator(self, pre=None):
      """Compute the two point correlator
         C_2(2) = <psi|S_z^1 S_z^2|psi> - (<psi|S_z^1|psi>)**2
      """
      brath, ketth = self.getbraket(pre, self.debug)
      pre = [brath, ketth]
      sz = Sig_z
      right = self.S1amp(sz, pre=pre)**2
      op2 = np.kron(sz, sz).reshape((2, 2, 2, 2))
      left = self.hamp(op2, pre=pre)
      return left - right

  def hamp(self, U, pre=None):
      brathL, ketthL = self.braketcontract()
      # brathR, ketthR = self.braketcontract(left=False)
      #brath, ketth = self.getbraket(pre, self.debug)
      # expect = scon((brath, U, ketth), ([1, 2, 3, 4], 
                                           # [2, 3, 
                                            # 5, 6], 
                                        # [1, 5, 6, 4]))
      
      expectL = scon((brathL, U, ketthL), ([1, 2, 3, 4], 
                                            [5, 6, 
                                             2, 3], 
                                         [1, 5, 6, 4]))
      # expectR = scon((brathR, U, ketthR), ([1, 2, 3, 4], 
                                            # [5, 6, 
                                             # 2, 3], 
                                         # [1, 5, 6, 4]))
      
      return expectL.real
  
  def S1amp(self, S, pre=None):
      brath, ketth = self.getbraket(pre, self.debug)
      expect = scon((brath, S, ketth), ([1, 2, 3, 4], 
                                           [2, 
                                            5],
                                        [1, 5, 3, 4]))
      return expect.real
  

def makedelta(shp):
    if shp[1:] != shp[:-1]:
      raise ValueError("Delta must be 'square'")
    delta = np.zeros(shp)
    idx = np.diag_indices(shp[0], len(shp))
    delta[idx] = 1.
    return delta

class iMPS_Evolver:
  """This class (functionally really a namespace) takes an iMPS
     and a two-site operator (a d x d x d x d array) upon construction.
     Method 'evolve' performs the necessary manipulations to produce
     the updated iMPS upon application of the operator. It then returns
     the new iMPS.
  """
  def __init__(self, mps, U):
      self.iMPS = mps
      self.U = U

  def evolve(self):
      big_theta = self.bigtheta()
      X, Y, newlamA = self.svd(big_theta)
      newlamA /= la.norm(newlamA)
      
      invB = np.reciprocal(self.iMPS.lambda_B)
      newGamA = (invB * X.T).T  #==np.einsum('ij, ikl', np.diag(invB), X)
      newGamB = Y*invB          #==np.einsum('ikl, lm', Y, np.diag(invB))
      return newGamA, newlamA, newGamB 

  def bigtheta(self):
      """Construct the fully contracted MPS "Theta". 
         Index arrangement is Th[alpha, i, j, gamma]
      """
      # net = self.iMPS.ketth
      # to_contract = (self.U, net)
      # idx = ([1, 2, -2, -3],
             # [-1, 1, 2, -4])
      # out = scon(to_contract, idx)
      left = (self.iMPS.lambda_B * self.iMPS.Gamma_A.T).T
      left *= self.iMPS.lambda_A
      right = self.iMPS.Gamma_B * self.iMPS.lambda_B
      to_contract = (self.U, left, right)
      idx = ([1, 3, -2, -3],
             [-1, 1, 2],
             [2, 3, -4])
      out = scon(to_contract, idx)
      return out

  def baretheta(self):
      lamAmat = np.diag(self.iMPS.lambda_A)
      lamBmat = np.diag(self.iMPS.lambda_B)
      to_contract = (lamBmat, self.iMPS.Gamma_A, lamAmat,
                     self.iMPS.Gamma_B, lamBmat)
      idx = ([-1, 1],
             [1, -2, 2],
             [2, 3],
             [3, -3, 4],
             [4, -4]
            )
      out = scon(to_contract, idx)
      return out


  def svd(self, theta):
      """Compute the singular value decomposition of theta, truncated
         to the maximum bond dimension.
      """
      chi = self.iMPS.chi
      d = self.iMPS.d
      flatdim = d*chi
      thflat = theta.reshape((flatdim, flatdim))
      # print np.allclose(np.ravel(theta), np.ravel(thflat))
      U, s, V = la.svd(thflat, full_matrices=True, compute_uv=True)
      #Truncate
      X = U[:, :chi].reshape((chi, d, chi))
      # print np.allclose(np.ravel(X), np.ravel(U[:, :chi]))
      Y = V[:chi, :].reshape((chi, d, chi))
      # print np.allclose(np.ravel(Y), np.ravel(V[:chi, :]))
      S = s[:chi]
      if DEBUG:
        print "[DEBUG] Truncation error: ", la.norm(S) - la.norm(s)
      return X, Y, S
      
def random_rng(shp, low=-0.5, high=0.5):
    return (high - low) * np.random.random_sample(shp) + low

def random_cmplx(shp, real_low=-0.5, real_high=0.5, imag_low=-0.5, imag_high=0.5):
    """Return a normalized randomized complex matrix of shape shp.
    """
    realpart = random_rng(shp, low=real_low, high=real_high)
    imagpart = 1.0j * random_rng(shp, low=imag_low, high=imag_high)
    bare = realpart + imagpart
    bare /= la.norm(bare)
    return bare

def initial_conditions(chi=CHI, d=D):
    lambda_A = random_rng(chi)
    lambda_A /= la.norm(lambda_A)
    lambda_B = random_rng(chi)
    lambda_B /= la.norm(lambda_B)
    Gamma_A = random_cmplx((chi, d, chi))
    Gamma_B = random_cmplx((chi, d, chi))
    my_network = iMPS(d, chi, chi, lambda_A, lambda_B, Gamma_A, Gamma_B)
    return my_network

def single_run(chi=CHI, h=TRANSVERSE_FIELD_FACTOR_h, plot=True):
    mps = initial_conditions(chi=chi)
    my_TEBD = iTEBD(mps, U_t, h=h)
    out = my_TEBD.evolve(T0, TF, DT)
    if plot:
      Eplt = plt.subplot2grid((4, 3), (0, 0), colspan=3)
      Eplt.plot(out["t"], out["E"],label=r"$\chi=$"+str(chi), lw=2.0)  
      exact = Utexactenergy(h=h)
      Eplt.axhline(exact, 0., 1., label="Exact", lw=2.0, color="black")
      Eplt.set_xlabel(r"$\tau$", fontsize=25)
      Eplt.set_ylabel("E", fontsize=25)
      Eplt.set_title("h = "+str(h), fontsize=25)
      Eplt.set_xlim((0, np.max(out["t"]))) 
      Eplt.legend(fontsize=20, loc="best")

      Sxplt = plt.subplot2grid((4,3), (1, 0), colspan=3)
      Sxplt.plot(out["t"], out["Sx"])
      Sxplt.set_xlabel(r"$\tau$", fontsize=25)
      Sxplt.set_ylabel("Sx", fontsize=25)
      Sxplt.set_xlim((0, np.max(out["t"]))) 
      
      Syplt = plt.subplot2grid((4,3), (2, 0), colspan=3)
      Syplt.plot(out["t"], out["Sy"])
      Syplt.set_xlabel(r"$\tau$", fontsize=25)
      Syplt.set_ylabel("Sy", fontsize=25)
      Syplt.set_xlim((0, np.max(out["t"]))) 
      
      Szplt = plt.subplot2grid((4,3), (3, 0), colspan=3)
      Szplt.plot(out["t"], out["Sz"])
      Szplt.set_xlabel(r"$\tau$", fontsize=25)
      Szplt.set_ylabel("Sz", fontsize=25)
      Szplt.set_xlim((0, np.max(out["t"]))) 
      plt.show()
      # C2plt = plt.subplot2grid((3, 3), (2, 0), colspan=3)
      # C2plt.plot(out["t"], out["C2"])
      # C2plt.set_xlabel(r"$\tau$", fontsize=25)
      # C2plt.set_ylabel("C2", fontsize=25)
      # C2plt.set_xlim((0, np.max(out["t"]))) 
      # plt.show()

def multiple_runs(chis, hs):
    outdict = dict()
    for chi in chis:
      outdict[chi] = dict()
      mps = initial_conditions(chi=chi)
      for h in hs:
        my_TEBD = iTEBD(mps, U_t, h=h)
        outdict[chi][h] = my_TEBD.evolve(T0, TF, DT)
    pickle.dump(outdict, open("energies.p", "wb"))

def load_plot(chis, hs):
    outdict = pickle.load( open("energies.p", "rb"))
    for h in hs:
      for chi in chis:
        thisdict = outdict[chi][h]
        plt.plot(thisdict[0], thisdict[1], label=r"$\chi=$"+str(chi), lw=2.0)
        exact = thisdict[2]
      plt.axhline(exact, 0., 1., label="Exact", lw=2.0)
      plt.xlim((0, 3.0))
      plt.xlabel(r"$\tau$", fontsize=25)
      plt.ylabel("E", fontsize=25)
      plt.title("h = "+str(h), fontsize=25)
      plt.legend(fontsize=20, loc="best")
      plt.savefig("h"+str(h)+".pdf")
      plt.show()

def timerun(mps, h):
    my_TEBD = iTEBD(mps, U_t, h=h)
    out = my_TEBD.evolve(0., 0.2, 0.1)

def wrapper(func, *args, **kwargs):
    def wrapped():
        return func(*args, **kwargs)
    return wrapped

def timeplot(chis, Nrep=20):
    import timeit 
    times = []
    for chi in chis:
      mps = initial_conditions(chi=chi)
      totime = wrapper(timerun, mps, 0.9)
      t = timeit.timeit(totime, number=Nrep)
      times.append(t/Nrep)
    print chis
    print times
    plt.plot(chis, times)
    plt.show()
    
if __name__=="__main__":
    # chis = range(5, 40, 5)
    # timeplot([4, 8, 16, 32, 64, 128])
    single_run()
    # chis = [4, 8, 16, 32, 64]
    # hs = [0.4, 0.8, 1.2, 1.6, 2.0]
    #multiple_runs(chis, hs)
    #load_plot(chis, hs)



