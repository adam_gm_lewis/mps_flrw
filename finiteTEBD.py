import utils
import numpy as np
from numpy import linalg as la
import matplotlib.pyplot as plot
from scon import scon

##############################################################################
#SIMULATION PARAMETERS
##############################################################################
CHI = 8   #the bond dimension
D   = 2   #the Hilbert space dimension
N   = 4   #number of nodes

T0 = 0.   #initial time
TF = 20.   #final time
DT = 0.01  #timestep
DT_OBS = 0.01 #how often to compute observables

#Physical variables
INTERACTION_FACTOR_J = 1.00    
TRANSVERSE_FIELD_FACTOR_h = 1.2 

#Pauli matrices
Sig_x = np.array([ [0, 1],
                   [1, 0]] )

Sig_y = np.array([ [0, -1.j],
                   [1.j, 0]] )

Sig_z = np.array([ [1,  0],
                   [0, -1]] )


class TEBD_evolver:
    """
      Class to handle the evolution (keep track of timesteps, output, etc).
    """
    def __init__(self, mps, H_func, hparms): 
      self.mps = mps
      self.H_func = H_func
      self.hparms = hparms

    def evolve(self, t0, tf, dt, dt_obs):
      t = t0
      print "BEGINNING EVOLUTION..."
      print "\t t0 = ", t, " | tf = ", tf, " | delta_t = ", dt
      outtime = dt_obs
      while t<tf:
        H = self.H_func(t, hparms) 
        U = utils.Uexp(-H*dt)
        self.mps.evolve(U)
        t+=dt
        if t >= outtime:
          print mpsnorm(self.mps)
          # obs = self.compute_observables(t, H)
          outtime += dt_obs


class MPS:
    """
      Class to represent the MPS of the chain. Contains also methods to 
      apply operators and compute expectation values.

      Members:
        N    -> the length of the chain. Also the number of gammas. Set equal
                to the number of gammas upon construction.
        chi  -> the bond dimension. Read from the shape of the gammas on 
                construction. That all gammas/lambdas have the appropriate 
                shapes is enforced.
        d    -> the physical dimension. Read from the shape of the gammas
                on construction.
        gams -> the N gamma tensors (numpy arrays). The first and last have
                shape (d, chi). The others have shape (d, chi, chi).
        lams -> the N-1 lambda vectors (numpy arrays). These have shape (chi).
   """

    def __init__(self, lams, gams, normalize=True):
        self.lams=lams
        self.gams=gams
        self.N = len(gams)
        
        if self.N < 3:
          raise ValueError("Need at least 3 gammas; input was " + str(self.N))

        if self.N-1 != len(lams):
          errstring = "Input had " + str(len(lams)) + " lambdas and \n"
          errstring += str(self.N) + "gammas. Should be exactly one less \n"
          errstring += "lambda than gammas."
          raise ValueError(errstring)

        self.chi = gams[0].shape[1] 
        self.d = gams[0].shape[0]
        
        # if gams[0].shape != (self.d, self.chi):
          # raise ValueError("gamma 0 had shape" + str(gams[0].shape) "; should"+
                    # "be ("+str(self.d)+","+str(self.chi)+")")
        
        # if gams[N].shape != (self.d, self.chi):
          # raise ValueError("gamma " + str(self.N) +" had shape" + 
                    # str(gams[0].shape) "; should be ("+str(self.d)+","
                    # +str(self.chi)+")")
       
        for i in range(0, self.N):
          if gams[i].shape != (self.d, self.chi, self.chi):
            raise ValueError("gamma " + str(i) +" had shape" + 
                      str(gams[i].shape) + "; should be ("+str(self.d)+","
                      +str(self.chi)+","+str(self.chi)+")")

        for i in range(0, self.N-1):
          if lams[i].shape != (self.chi,):
            raise ValueError("lambda " + str(i) +" had shape" + 
                      str(lams[i].shape) + "; should be ("+str(self.chi)+")")

        if normalize:
          self.normalize()

    #########################################################################
    #METHODS FOR TEBD EVOLUTION
    #########################################################################
    def applyfromj(self, U, j):        
        for n in range(j, self.N-1, 2):
            th = self.contracttwosite(U, n)
            X, Y, S = self.svd(th)
            self.gams[n] = X
            self.gams[n+1] = Y
            self.lams[n] = S
    
    def evolve(self, U):
        """
          Apply the two-site operator U to the entire chain.
        """
        self.applyfromj(U, 0) #odd (since 1-based in paper)
        self.applyfromj(U, 1) #even
        self.normalize()


    def contracttwosite(self, U, n):
        """Applies a two-site operator U to the nth and n+1th bond. The result
           is a single tensor Th(A_1, j_1, A_2, j_2), where the A's (j's) run
           over the virtual (physical) index. In the case of n=0 or N, A_1
           or A_2 respectively will have dimension one 
           (but the tensor will still be reshaped to have "rank four").
        """
        if n >= self.N:
            raise ValueError("Tried to contract " + str(n) + "th site out of " +
                             str(self.N))
        gam1 = self.gams[n]
        lam = self.lams[n]
        gam2 = self.gams[n+1]

        left = gam1*lam
        to_contract = (U, left, gam2)
        idx = ([1, 3, -2, -4], #U
               [1, -1, 2],    #left
               [3, 2, -3])    #right 
        out = scon(to_contract, idx)
        dim = self.chi*self.d
        out = out.reshape((dim, dim))
        return out
    
    def svd(self, th, left=False, right=False):
        """Computes the singular value decomposition of the input matrix
           'th', which should be the (chi*d x chi*d) result of a contraction
           between two nodes (and, possibly, a two-site operator). The results
           are then truncated to the bond dimension chi, reshaped into the
           appropraite gammas and lambdas, and returned in the order
           gamma_n, gamma_n+1, lambda_n.
        """

        chi = self.chi
        d = self.d
        if th.shape != (chi*d, chi*d):
            raise ValueError("Input matrix th had shape "
                  +str(th.shape)+", but shape must be ("
                  +str(chi*d)+","+str(chi*d)+"); i.e. (chi*d, chi*d).")

        U, s, V = la.svd(th, full_matrices=True, compute_uv=True)
        
        X = U[:, :chi].reshape((d, chi, chi))
        Y = V[:chi, :].reshape((d, chi, chi))
        S = s[:chi]
        return X, Y, S
    
    def normalize(self):
        norm = mpsnorm(self)
        self.gams[0] /= np.sqrt(norm)
    
############################################################################## 
#FUNCTIONS TO COMPUTE EXPECTATION VALUES
############################################################################## 
def mpsnorm(mps, onesite=None, nsite=None):
    """Computes the norm of the MPS. First, each lambda is contracted with
       the gamma to its immediate left and the conjugate of the resulting
       chain is computed. 

       If the optional arguments are specified, this function can also be 
       used to compute expectation values of single-site operators. The 
       operator should be sent in as a (d x d) matrix to argument onesite,
       and the node upon which it is to act to the argument nsite.
    """

    #first contract all the lambdas with the gammas
    As = []
    for i in range(0, mps.N-1):
      As.append(mps.gams[i]*mps.lams[i])
    As.append(mps.gams[mps.N-1])


    # Make the conjugate
    Adags = [np.conj(a) for a in As]

    if onesite is not None:
      if nsite is None:
        raise ValueError(
            "onesite and nsite must either both be specified or neither.")
      elif nsite > mps.N:
        raise ValueError("nsite = " + str(nsite) + " was too high.")
      elif nsite < 0:
        raise ValueError("nsite = " + str(nsite) + " was too low.")

      thisA = As[nsite]
      to_contract = ([thisA, onesite])
      idx = ([1, -2, -3],
             [1, -1]
            )
      thisA = scon(to_contract, idx)
      As[nsite] = thisA
 
    norm = netcontract(As, Adags)
    return norm.real

def netcontract(As, Adags):
    """Contracts a set of tensors As with Adags. If Adags are the conjugates
       of As, this returns the norm of the MPS. One may also use this 
       function to compute arbitrary expectation values, by sending in an As 
       differing from Adags by the action of some operator.
    """
    N = len(As)
    if N != len(Adags):
      raise ValueError("As (len "+str(N)+ ") and Adags (len " + 
          str(len(Adags)) + ") must have same length.")

    left = topbotcon(As[0], Adags[0])
    for i in range(1, N-1):
      left = leftrightcon(left, As[i])
      left = topbotcon(left, Adags[i])
    left = leftrightcon(left, As[N-1])
    
    to_contract = (left, Adags[N-1])
    idx = (
            [1, 2, 3],
            [1, 2, 3]
          )
    norm = scon(to_contract, idx)
    return norm

def topbotcon(top, bot):
    """Contracts a node in the MPS with its conjugate.
       Result has shape (chi, chi)
    """
    to_contract = (top, bot)
    idx = ( 
            [1, 2, -1],
            [1, 2, -2]
          )
    result = scon(to_contract, idx)
    return result 

def leftrightcon(left, right):
    """Contracts a contracted node-conjugate pair with the node to its
       right.
       Result has shape (d, chi, chi)
    """
    to_contract = (left, right)
    idx = (
            [1, -2],
            [-1, 1, -3]
          )
    result = scon(to_contract, idx)
    return result

############################################################################## 
#FUNCTIONS TO INITIALIZE THE MPS
############################################################################## 
def randommps(d, chi, n, rmin=-0.5, rmax=0.5, imin=-0.5, imax=0.5):
    """
      Returns a random (normalized) MPS with physical dimension d and 
      bond dimension chi. The MPS has n nodes, and thus n gamma tensors and
      n-1 lambda vectors. The lambdas are filled with random real numbers between
      imin and imax, then normalized. The gammas are filled with random 
      complex numbers with real parts ranging from rmin to rmax and imaginary 
      parts ranging from imin to imax. The network is then normalized using
      the method mps.normalize() (which divides the 0th gamma by the square 
      root of its L2 norm).

      The 0th and nth gamma need to be handled specially, since they have only
      one nontrivial virtual index. In these cases we create a random (d, chi)
      matrix and tile it into a (d, chi, chi) tensor, such that the 1st 
      (2nd) index refers to a tiling of the original random matrix (i.e.
      (:, i, :) for the 0th gamma and (:, :, i) for the nth are the same
      matrix for all i).
    """
    gamshape = (d, chi, chi)
    edgeshape = (d, chi)
    gams = []
    lams = []

    #First make the 0th and nth gammas. 
    gam0vals = utils.random_cmplx(edgeshape)
    gam0 = np.zeros(gamshape, dtype=np.complex)
    for i in range(0, chi):
      gam0[:, i, :] = gam0vals
    
    gamnvals = utils.random_cmplx(edgeshape, real_low=rmin, real_high=rmax,
                                  imag_low=imin, imag_high=imax)
    gamn = np.zeros(gamshape, dtype=np.complex)
    for i in range(0, chi):
      gamn[:, :, i] = gamnvals

    #Now make the gamma chain.
    gams.append(gam0)
    for i in range(1, n-1):
      gam = utils.random_cmplx(gamshape, real_low=rmin, real_high=rmax,
                               imag_low=imin, imag_high=imax)
      gams.append(gam) 
    gams.append(gamn)

    #Now the lambda chain.
    for i in range(0, n-1):
      lam = utils.random_rng((chi))
      lam /= la.norm(lam)
      lams.append(lam)
   
    #Initialize the mps.
    mps = MPS(lams, gams) 
    return mps

if __name__=="__main__":
    hparms = (INTERACTION_FACTOR_J, TRANSVERSE_FIELD_FACTOR_h)
    mps = randommps(D, CHI, N)
    tebd = TEBD_evolver(mps, utils.H_ising, hparms)
    tebd.evolve(T0, TF, DT, DT_OBS)
