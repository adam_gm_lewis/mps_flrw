"""
  This file stores utility functions useful to many different MPS 
  implementations.
"""
import numpy as np
from scipy import linalg as spla
from numpy import linalg as la

Sig_x = np.array([ [0, 1],
                   [1, 0]] )

Sig_y = np.array([ [0, -1.j],
                   [1.j, 0]] )

Sig_z = np.array([ [1,  0],
                   [0, -1]] )

def Uexp(U):
    """Compute exp(U).
       Where U has shape (2, 2, 2, 2).
    """
    Umat = U.reshape((4, 4))
    Umatexp = spla.expm(Umat)
    Uexp = Umatexp.reshape((2, 2, 2, 2,))
    return Uexp

def H_ising(t, hparms):
    J = hparms[0]
    h = hparms[1]
    H = -J * (np.kron(Sig_x, Sig_x) + h * np.kron(Sig_z, np.eye(2)))
    Hshpd = H.reshape((2,2,2,2))
    Hshpd = np.transpose(Hshpd, (2,3,0,1) )
    return Hshpd

def random_rng(shp, low=-0.5, high=0.5):
    return (high - low) * np.random.random_sample(shp) + low

def random_cmplx(shp, real_low=-0.5, real_high=0.5, imag_low=-0.5, imag_high=0.5):
    """Return a normalized randomized complex matrix of shape shp.
    """
    realpart = random_rng(shp, low=real_low, high=real_high)
    imagpart = 1.0j * random_rng(shp, low=imag_low, high=imag_high)
    bare = realpart + imagpart
    bare /= la.norm(bare)
    return bare
